package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		GridPane root = new GridPane();
		final int size = 8;
		
		for (int col = 0; col < size; col++) {
			for (int row = 0; row < size; row++) {
				Rectangle r = new Rectangle();
				Color c;
				if ((row+col)%2 == 0) c = Color.BLACK;
				else c = Color.WHITE;
				r.setFill(c);
				root.add(r, col, row);
				r.widthProperty().bind(root.widthProperty().divide(size));
                r.heightProperty().bind(root.heightProperty().divide(size));
			}
		}
		
		primaryStage.setScene(new Scene(root, 800, 800));
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
